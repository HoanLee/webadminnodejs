var express=require("express");
var router= express.Router();

var user_md=require("../models/users");

var helper=require("../helpers/helper");

var database=require("../common/database");

router.get("/list",function(req,res){
    res.render("admin/dashboard",{data:{error:"error"}});
});

router.post("/list",function(req,res){
    res.render("admin/dashboard",{data:{error:"error"}});
});

router.get("/student",function(req,res){
    res.render("admin/student",{data:{error:"error"}});
});

router.post("/student",function(req,res){
    res.render("admin/student",{data:{error:"error"}});
});
router.get("/signup",function(req,res){
    res.render("signup",{data:{}});
});
router.post("/signup",function(req,res){
    var user=req.body;
    if(user.username.trim().length==0){
        res.render("signup",{data:{error:"Username is required."}})
    }
    if(user.password!=user.passwordrepeat && user.password.trim().length!=0){
        res.render("signup",{data:{error:"Passwork is not match."}});
    }

    var password=helper.hash_password(user.password);
    user={
        username: user.username,
        password: password
    };

    var result=user_md.addUser(user);
    if(result){
        res.json({message:"insert success"})
    }else{
        res.render("signup",{data:{error:"err"}});
    } 
});


router.get("/signin",function(req,res){
    res.render("signin",{data:{}});
});
router.post("/signin",function(req,res){
    var params= req.body;

    if(params.username.trim().length==0){
        res.render("signin",{data:{error:"Please enter username."}});
    }else{
        var data =user_md.login(params.username);
        if(data){
            data.then(function(users){
                var user=users[0];
                var status= helper.compare_password(params.password,user.password);
                if(!data){
                    res.render("signin",{data:{error:"Password is not correct"}});
                }else{
                    res.redirect("/admin/");
                }
            });
        }else{
            res.render("signin",{data:{error:"User not exists "}});
        }
    }
});

module.exports=router;